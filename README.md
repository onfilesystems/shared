Introduction
============

That's a package of commonly used constants, functions and other utils for Onfile Systems ecosystem.

### Installation (in browser usage)
1. ```bower install git@bitbucket.org:onfilesystems/shared.git```
2. Within browser all exported functionality is accessible through ```window.onfileShared``` or simply
```onfileShared```  property.

### Installation (node.js server side or in webpack usage)
1. ```npm i --save https://MEGApixel23@bitbucket.org/onfilesystems/shared.git```
2. Within your node code ```import { typeOfNotarizationOptions, utils } from 'shared'``` or in require style
```const { typeOfNotarizationOptions, utils } = require('shared');```

### Development
After needed functionality, constants and functions are added into the package run ```yarn build```
that builds the package for a browser usage, increase a package version and push built files into the repository.