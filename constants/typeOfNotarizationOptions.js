module.exports = [
  { text: 'Acknowledgement', value: 1 },
  { text: 'Jurat', value: 2 },
  { text: 'Verification', value: 3 },
  { text: 'Protest', value: 4 },
  { text: 'Deposition', value: 5 },
  { text: 'Oath/Affirmation', value: 6 }
];