const KBA_ID_EVIDENCE = 1;
const PERSONAL_KNOWLEDGE_EVIDENCE = 2;

module.exports = {
  KBA_ID_EVIDENCE,
  PERSONAL_KNOWLEDGE_EVIDENCE,
  options: [
    { text: 'Identity proofing & credential analysis', value: KBA_ID_EVIDENCE },
    { text: 'Personal knowledge of principal', value: PERSONAL_KNOWLEDGE_EVIDENCE }
  ]
}
