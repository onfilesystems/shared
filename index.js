const utils = require('./utils');
const idEvidence = require('./constants/idEvidence');
const typeOfNotarizationOptions = require('./constants/typeOfNotarizationOptions');

module.exports = {
  utils,
  idEvidence,
  typeOfNotarizationOptions,
};
